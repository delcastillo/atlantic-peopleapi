﻿using Microsoft.AspNetCore.Mvc;
using PeopleAPI.Models;
using PeopleAPI.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeopleAPI.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ModelBaseController : ControllerBase {
        public ModelBaseController(PeopleDBContext dbContext) {
            this.dbContext = dbContext;
        }

        protected PeopleDBContext dbContext { get; set; }

        protected ObjectResult BuildResponseObjectResult(MSGenericResponse res, int code = 0) {
            return StatusCode(code != 0 ? code : res.CheckStatus(), res);
        }
    }
}
