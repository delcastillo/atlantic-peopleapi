﻿using Microsoft.AspNetCore.Mvc;
using PeopleAPI.Models;
using PeopleAPI.Models.Entities;
using PeopleAPI.Models.Entities.PersonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeopleAPI.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ModelBaseController {

        public PeopleController(PeopleDBContext dbContext)
        : base(dbContext) {
        }

        [HttpGet, Route("Delete/{ID:int}")]
        public ObjectResult Delete([FromRoute] int ID){
            MSGenericResponse response = new MSGenericResponse() {
                Status = "Success",
                Message = "Data was deleted from the database successfully"
            };
            try {
                Person per = dbContext.People.Find(ID);
                if (per == null) {
                    response.Status = "Error";
                    response.Message = "Data was not found";
                    return BuildResponseObjectResult(response);
                }
                dbContext.People.Remove(per);
                dbContext.SaveChanges();
            } catch (Exception e) {
                response.SetErrorInfo(e);
            }
            return BuildResponseObjectResult(response);
        }

        [HttpGet,Route("getPeople")]
        public ObjectResult getPeople() {
            MSGenericResponse response = new MSGenericResponse() {
                Status = "Success",
                Message = "Data was fetched from the database successfully"
            };
            try {
                List<MPeople> people = dbContext.People.Select(s => new MPeople() {
                    ID                      = s.ID,
                    IdentificationNumber    = s.IdentificationNumber,
                    FirstName               = s.FirstName,
                    LastName                = (s.LastName == "N/A"? "": s.LastName),
                    FirstSurName            = s.FirstSurName,
                    LastSurName             = s.LastSurName,
                    NumberContact           = s.NumberContact,
                    Email                   = s.Email,
                    Bitth                   = s.Bitth,
                    Sure                    = s.Sure
                }).ToList();
                response.Data = people;
            } catch (Exception e) {
                response.SetErrorInfo(e);
            }
            return BuildResponseObjectResult(response);
        }

        [HttpPost, Route("Save")]
        public ObjectResult Save([FromBody] MPeople data) {
            MSGenericResponse response = new MSGenericResponse() {
                Status = "Success",
                Message = "Data saved successfully"
            };
            try {
                if (data == null) {
                    response.Status = "Error";
                    response.Message = "There is no information to work";
                    response.Data = null;
                    return BuildResponseObjectResult(response, (int)System.Net.HttpStatusCode.BadRequest);
                }
                Person per = null;
                if (data.ID.HasValue) {
                    per = dbContext.People.First(w => w.ID == data.ID);
                    per.IdentificationNumber    = data.IdentificationNumber;
                    per.FirstName               = data.FirstName;
                    per.LastName                = data.LastName;
                    per.FirstSurName            = data.FirstSurName;
                    per.LastSurName             = data.LastSurName;
                    per.NumberContact           = data.NumberContact;
                    per.Email                   = data.Email;
                    per.Bitth                   = data.Bitth;
                    per.Sure                    = data.Sure;
                } else {
                per = new Person() {
                    IdentificationNumber    = data.IdentificationNumber,
                    FirstName               = data.FirstName,
                    LastName                = data.LastName,
                    FirstSurName            = data.FirstSurName,
                    LastSurName             = data.LastSurName,
                    NumberContact           = data.NumberContact,
                    Email                   = data.Email,
                    Bitth                   = data.Bitth,
                    Sure                    = data.Sure
                };
                dbContext.People.Add(per);
                }
                dbContext.SaveChanges();
                response.Data = per.ID;
            } catch (Exception e) {
                response.SetErrorInfo(e);
            }
            return BuildResponseObjectResult(response);
        }
    }
}
