﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeopleAPI {
    public class GlobalData {
        public string[] CORSHostList { get; set; }

        public string UseConnection { get; set; }

        public bool EnableVervose { get; set; }

        private static readonly object padlock = new object();

        GlobalData() { }

        private static GlobalData current = null;

        public static GlobalData Current {
            get {
                if (current == null) {
                    lock (padlock) {
                        if (current == null) {
                            current = new GlobalData();
                        }
                    }
                }
                return current;
            }
        }
    }
}
