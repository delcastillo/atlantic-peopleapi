﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PeopleAPI.Models.Entities
{
    public partial class Person
    {
        [Key]
        public int ID { get; set; }
        public int IdentificationNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstSurName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastSurName { get; set; }
        [Required]
        [StringLength(50)]
        public string NumberContact { get; set; }
        [Required]
        [StringLength(50)]
        public string Email { get; set; }
        [Required]
        [StringLength(50)]
        public string Bitth { get; set; }
        public double Sure { get; set; }
    }
}
