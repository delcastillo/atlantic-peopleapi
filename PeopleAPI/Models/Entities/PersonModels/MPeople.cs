﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeopleAPI.Models.Entities.PersonModels {
    public class MPeople {
        public int? ID { get; set; }
        public int IdentificationNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstSurName { get; set; }
        public string LastSurName { get; set; }
        public string NumberContact { get; set; }
        public string Email { get; set; }
        public string Bitth { get; set; }
        public double Sure { get; set; }


    }
}
